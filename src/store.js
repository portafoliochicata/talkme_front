import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    current_component : 'setup',
    endpoint : {
      chatroom: process.env.VUE_APP_SOCKET + "/chats/proto_1/",
      logging: process.env.VUE_APP_HTTP + '/authentication/logging/',
      connection: process.env.VUE_APP_SOCKET + '/connection/',
      friend_connecting: process.env.VUE_APP_SOCKET + '/user-talkme/friend-connecting/',
    },
    authentication: {
      token: sessionStorage.getItem('talkme_token') || '',
      id: sessionStorage.getItem('talkme_id')  || -1,
    }
  },
  mutations: {
    UpdateCurrentComponent (state, component) {
      state.current_component = component;
      return state.current_component
    },
    UpdateAuthentication (state, token){
      state.authentication.token = token;
      return state.authentication.token;
    },
    GetAuthentication (state ){
      return state.authentication.token;
    },
    UpdateIdTalkUser (state, id){
      state.authentication.id = id;
      return state.authentication.id
    },
    GetIdTalkUser (state, id){
      return state.authentication.id;
    }

  },
  actions: {

  }
})
