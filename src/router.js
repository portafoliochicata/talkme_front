import Vue from 'vue'
import Router from 'vue-router'
import logging from './components/logging'
import dashboard from './components/dashboard'

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'loggin',
      component: logging
    },
    {
      path: '/main-screen/',
      name: 'dashboard',
      component: dashboard
    },
  ]
})
